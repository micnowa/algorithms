package learning.algorithms.sorting;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class MergeSortTest {
    private int[] tab;
    private int[] tabReversedOrder;
    MergeSort mergeSort;

    @Before
    public void setUp() {
        tab = new int[]{38, 27, 43, 3, 9, 82, 10,20};
        tabReversedOrder = new int[]{10,9,8,7,6,5,4,3,2,1};
        mergeSort = new MergeSort();
    }

    @Test
    public void sortsArray() {
        int[] sorted = mergeSort.sort(tab);
        assertArrayEquals(new int[]{3,9,10,20,27,38,43,82}, sorted);
    }

    @Test
    public void sortsReversedOrderArray() {
        int[] sorted = mergeSort.sort(tabReversedOrder);
        assertArrayEquals(new int[]{1,2,3,4,5,6,7,8,9,10}, sorted);
    }

}