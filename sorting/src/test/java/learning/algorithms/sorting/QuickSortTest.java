package learning.algorithms.sorting;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class QuickSortTest {
    private List<Integer> integers;
    private List<Integer> results;
    private QuickSort<Integer> quickSort;
    private int size;
    private int tab[];

    @Before
    public void setUp() {
        size = 100000;
        integers = new ArrayList<>(size);
        results = new ArrayList<>(size);
        tab = new int[size];
    }

    @Test
    public void sortsReversedOrderElements() {
        for (int ii = size; ii > 0; ii--) {
            integers.add(ii);
            results.add(size + 1 - ii);
            tab[ii - 1] = size + 1 - ii;
        }

        quickSort = new QuickSort<>(integers, Integer::compareTo);
        quickSort.doQuickSort();
        assertEquals(integers, results);

    }


}