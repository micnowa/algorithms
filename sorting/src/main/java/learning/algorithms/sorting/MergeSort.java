package learning.algorithms.sorting;

import java.util.Arrays;
import java.util.Stack;

public class MergeSort {
    private int tab[];
    private int size;

    public int[] sort(int[] table) {
        if (table.length == 1) return table;
        tab = table;
        size = tab.length;
        int[] tab1 = Arrays.copyOfRange(tab, 0, tab.length / 2);
        int[] tab2 = Arrays.copyOfRange(tab, tab.length / 2, tab.length);
        tab1 = sort(tab1);
        tab2 = sort(tab2);

        int newSize = tab1.length + tab2.length;
        int[] newTab = new int[newSize];

        Stack<Integer> st1 = new Stack<>();
        Stack<Integer> st2 = new Stack<>();

        for (int i = 0; i < tab1.length; i++)
            st1.add(tab1[tab1.length - i - 1]);
        for (int i = 0; i < tab2.length; i++)
            st2.add(tab2[tab2.length - i - 1]);

        for (int i = 0; i < newTab.length; i++) {
            if (st1.empty())
                newTab[i] = st2.pop();
            else if (st2.empty())
                newTab[i] = st1.pop();
            else
                newTab[i] = st1.peek() < st2.peek() ? st1.pop() : st2.pop();
        }
        return newTab;
    }
}
