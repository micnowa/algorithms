package learning.algorithms.sorting;

import java.util.Comparator;
import java.util.List;

public class QuickSort<E> {
    private List<E> list;
    private Comparator<E> comparator;

    public QuickSort(List<E> table, Comparator<E> comparator) {
        this.list = table;
        this.comparator = comparator;
    }

    public void doQuickSort() {
        quickSort(0, list.size() - 1);
    }

    private void quickSort(int left, int right) {
        int i, j;
        i = left;
        j = right;
        E pivot = list.get((left + right) / 2);
        while (i <= j) {
            while (comparator.compare(list.get(i), pivot) < 0)
                i++;
            while (comparator.compare(pivot, list.get(j)) < 0)
                j--;
            if (i <= j) {
                swapElements(i, j);
                i++;
                j--;
            }
        }
        if (left < j)
            quickSort(left, j);
        if (i < right)
            quickSort(i, right);
    }

    private void swapElements(int i, int j) {
        E tmp;
        tmp = list.get(i);
        list.set(i, list.get(j));
        list.set(j, tmp);
    }
}
