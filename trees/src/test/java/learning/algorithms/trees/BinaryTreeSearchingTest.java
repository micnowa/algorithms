package learning.algorithms.trees;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class BinaryTreeSearchingTest {
    private BinaryTree tree;

    @Before
    public void setUp() {
        tree = new BinaryTree(11);
        tree.add(6);
        tree.add(19);
        tree.add(4);
        tree.add(8);
        tree.add(17);
        tree.add(43);
        tree.add(5);
        tree.add(10);
        tree.add(31);
        tree.add(49);
        tree.add(55);
    }

    @Test
    public void searchesNode(){
        assertEquals(31, tree.search(31).value);
    }

    @Test
    public void searchesNodesParent(){
        assertEquals(43, tree.searchParent(31).value);
    }

    @Test
    public void returnsNullWhenNodeDoesNotExist(){
        assertNull(tree.search(100));
    }

    @Test
    public void findMinimumValue() {
        assertEquals(4, tree.findMinValue().value);
    }
}
