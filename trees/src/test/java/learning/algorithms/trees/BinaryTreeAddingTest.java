package learning.algorithms.trees;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class BinaryTreeAddingTest {
    private BinaryTree tree;
    private BinaryTree treeDuplicate;

    @Before
    public void setUp() {
        tree = new BinaryTree(11);
        tree.add(6);
        tree.add(19);
        tree.add(4);
        tree.add(8);
        tree.add(17);
        tree.add(43);
        tree.add(5);
        tree.add(10);
        tree.add(31);
        tree.add(49);

        treeDuplicate = new BinaryTree(11);
        treeDuplicate.add(6);
        treeDuplicate.add(19);
        treeDuplicate.add(4);
        treeDuplicate.add(8);
        treeDuplicate.add(17);
        treeDuplicate.add(43);
        treeDuplicate.add(5);
        treeDuplicate.add(10);
        treeDuplicate.add(31);
        treeDuplicate.add(49);
        treeDuplicate.add(50);
    }

    @Test
    public void addsNode() {
        tree.add(50);
        assertTrue(tree.compare(treeDuplicate));
    }

}
