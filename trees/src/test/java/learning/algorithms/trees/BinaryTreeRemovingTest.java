package learning.algorithms.trees;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class BinaryTreeRemovingTest {
    private BinaryTree tree;

    @Before
    public void setUp() {
        tree = new BinaryTree(11);
        tree.add(6);
        tree.add(19);
        tree.add(4);
        tree.add(8);
        tree.add(17);
        tree.add(43);
        tree.add(5);
        tree.add(10);
        tree.add(31);
        tree.add(49);
    }

    @Test
    public void removesLeafNode(){
        BinaryTree treeComp = new BinaryTree(11);
        treeComp.add(6);
        treeComp.add(19);
        treeComp.add(4);
        treeComp.add(8);
        treeComp.add(17);
        treeComp.add(43);
        treeComp.add(5);
        treeComp.add(10);
        treeComp.add(31);

        tree.removeNode(49);
        assertTrue(tree.compare(treeComp));

    }

    @Test
    public void removesNodeWithOneChild(){
        BinaryTree treeComp = new BinaryTree(11);
        treeComp.add(6);
        treeComp.add(19);
        treeComp.add(8);
        treeComp.add(17);
        treeComp.add(43);
        treeComp.add(5);
        treeComp.add(10);
        treeComp.add(31);
        treeComp.add(49);

        tree.removeNode(4);
        assertTrue(tree.compare(treeComp));

    }

    @Test
    public void removesNodeWithTwoChildren(){
        BinaryTree treeComp = new BinaryTree(11);
        treeComp.add(6);
        treeComp.add(31);
        treeComp.add(4);
        treeComp.add(8);
        treeComp.add(17);
        treeComp.add(43);
        treeComp.add(5);
        treeComp.add(10);
        treeComp.add(49);

        tree.removeNode(19);
        assertTrue(tree.compare(treeComp));

    }
}
