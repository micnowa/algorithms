package learning.algorithms.trees;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class BinaryTreeComparingTest {
    private BinaryTree tree;
    private BinaryTree treeDuplicate;

    @Before
    public void setUp() {
        tree = new BinaryTree(11);
        tree.add(1);
        tree.add(10);
        tree.add(99);
        tree.add(100);
        tree.add(50);
        tree.add(77);
        tree.add(2);
        tree.add(9);
        tree.add(8);
        tree.add(49);

        treeDuplicate = tree;
    }

    @Test
    public void comparesTrees(){
        assertTrue(tree.compare(treeDuplicate));
    }
}
