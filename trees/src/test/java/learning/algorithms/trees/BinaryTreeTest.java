package learning.algorithms.trees;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({
        BinaryTreeAddingTest.class,
        BinaryTreeComparingTest.class,
        BinaryTreeRemovingTest.class,
        BinaryTreeSearchingTest.class
})
public class BinaryTreeTest {
}