package learning.algorithms.trees;

public class BinaryTree {
    private Node root;

    public BinaryTree(int value) {
        root = new Node(value);
    }

    public void add(int value) {
        root.add(value);
    }

    public Node search(int val) {
        return searchNode(root, val);
    }

    public Node searchParent(int val) {
        return searchParent(root, val);
    }

    public boolean compare(BinaryTree tree) {
        return compare(root,tree.root);
    }

    public Node findMinValue() {
        return findMinValue(root);
    }

    public void removeNode(int val) {
        removeNode(root, val);
    }

    private Node searchNode(Node node, int val) {
        if (val == node.value) return node;
        Node nodeChosen = val > node.value ? node.right : node.left;
        return nodeChosen == null ? null : searchNode(nodeChosen, val);
    }

    private void removeNode(Node node, int val) {
        Node parent = searchParent(root, val);
        if (parent == null) return;
        Node child = searchNode(node, val);
        if(child == null) return;
        boolean childRight = parent.right == child;
        if (child.left == null && child.right == null) {
            if (childRight) parent.right = null;
            else parent.left = null;
        } if (child.left == null || child.right == null) {
            if (childRight) parent.right = child.right == null ? child.left : child.right;
            else parent.left = child.right == null ? child.left : child.right;
        } else {
            int value = findMinValue(child.right).value;
            removeNode(searchNode(root, value), value);
            child.value = value;
        }
    }

    private Node searchParent(Node node, int val) {
        if (val == node.right.value || val == node.left.value) return node;
        Node nodeChosen = val > node.value ? node.right : node.left;
        return searchParent(nodeChosen, val);
    }

    private Node findMinValue(Node node) {
        return node.left == null ? node : findMinValue(node.left);
    }

    private boolean compare(Node node1, Node node2) {
        if(node1 == null && node2 == null) return true;
        if(node1 == null || node2 == null) return false;
        if(node1.value != node2.value) return false;
        return compare(node1.left, node2.left) && compare(node1.right, node2.right);
    }

}
