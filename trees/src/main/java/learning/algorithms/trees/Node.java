package learning.algorithms.trees;

class Node {
    int value;
    Node right;
    Node left;

    Node(int value) {
        this.value = value;
    }

    void add(int val) {
        if (val > value) {
            if (right == null) {
                right = new Node(val);
                return;
            }
            right.add(val);
        } else {
            if (left == null) {
                left = new Node(val);
                return;
            }
            left.add(val);
        }
    }
}
